import Fluent
import Vapor

func routes(_ app: Application) throws {
//    app.get { req in
//        req.view.render("index", [
//            "title": "My Page",
//            "description": "This is my own page.",
//            "content": "Welcome to my page!"
//        ])
//    }
//    app.get("hello") { req in
//        return "Hello, world!"
//    }

    let todoController = TodoController()
    app.get("", use: todoController.index)
    app.post("", use: todoController.create)
    app.on(.DELETE, "", ":todoID", use: todoController.delete)
}
