import Fluent
import Vapor

struct TodoController {
    func index(req: Request) throws -> EventLoopFuture<View> {
        return Todo.query(on: req.db).all().flatMap{todos in
            let context = Context(todos: todos)
            return req.view.render("todos", context)
            
        }
    }
    
    func create(req: Request) throws -> EventLoopFuture<View> {
        let todo = try req.content.decode(Todo.self)
        return todo.save(on: req.db).flatMap {
            return Todo.query(on: req.db).all().flatMap{todos in
                let context = Context(todos: todos)
                return req.view.render("todos", context)
                
            }
        }
    }
    
    func delete(req: Request) throws -> EventLoopFuture<HTTPStatus> {
        return Todo.find(req.parameters.get("todoID"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { $0.delete(on: req.db) }
            .map { .ok }
    }
}

struct Context: Codable {
    var todos: [Todo]
}
